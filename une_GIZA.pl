#!/usr/bin/perl

# Este programa é um software livre; você pode redistribui-lo e/ou 
# modificá-lo dentro dos termos da Licença Pública Geral GNU como 
# publicada pela Fundação do Software Livre (FSF); na versão 2 da 
# Licença, ou (na sua opnião) qualquer versão.
# Este programa é distribuído na esperança que possa ser útil, 
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer
# MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, escreva para a Fundação do Software
# Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Programa une_GIZA
# Entrada: dois arquivos alinhados lexicalmente por GIZA++ e 
#          uma das opcoes de metodos de simetrizacao (i,u,r ou r1)
# Saida: Imprime na tela a uniao (u) OU a interseccao (i) OU o produto do refinamento (r ou r1)
# dos alinhamentos produzidos por GIZA++ 
# Funcao: Faz a simetrizacao dos alinhamentos aplicando um de 4 metodos definidos
# como parametro de entrada: u (uniao), i (interseccao), r (refinamento) e r1 (refinamento1).
# O refinamento esta baseado em (Och & Ney, 2003).
# OBS.: Para mais informacoes de como cada metodo de simetrizacao funciona, leia LEIA_ME


use strict;
use locale;

if ($#ARGV < 2) {
    print "Uso: $0 <alinhado1> <alinhado2> <i|u|r|r1>\n";
    print "Copyright (C) 2010-2012  PorTAl (www.lalic.dc.ufscar.br/portal)\n";
    exit 1;
};

my ($entrada1,$entrada2,$op,$linha1,$linha2,$prob1,$prob2,@tok1,@ali1,@ali2,@aux,$i);

$entrada1 = shift(@ARGV);
$entrada2 = shift(@ARGV);
$op = shift(@ARGV);

open(IN1,$entrada1) or die "Nao eh possivel abrir o arquivo $entrada1\n";
open(IN2,$entrada2) or die "Nao eh possivel abrir o arquivo $entrada2\n";
$linha1 = <IN1>;
$linha2 = <IN2>;
while ($linha1) {
	if ($linha1 =~ /(\# Sentence [^\:]+)\:\s*(.+)\n/) {
		$linha1 = $1;
		$prob1 = $2;
		if ($linha2 =~ /Sentence [^\:]+\:\s*(.+)\n/) { $prob2 = $1; }
		# Faz um merge das probabilidades dos dois alinhamentos de acordo com o metodo de simetrizacao
		print $linha1,": ",simetriza_probabilidades($prob1,$prob2,$op),"\n";
	}
	elsif ($linha1 =~ /^NULL/) {
		converte_linha_alinhamento($linha1,\@ali1,\@tok1);
		converte_linha_alinhamento($linha2,\@ali2);
		inverte_sentido_alinhamento(\@ali2);
		# Exemplo:
		# @ali1 = (7,0,2,3,4,5,1_6_8) e @ali2 = (6_7,1,2,3,4,5,8)
		
#		print "Ali1: ",join(" ",@ali1),"\n";
#		print "Ali2: ",join(" ",@ali2),"\n";
				
		@aux = (); # vai conter o alinhamento final
		if ($op eq "u") {
			for($i=0;$i <= $#ali1;$i++) {
				if ($ali1[$i] eq "0") { $aux[$i] = $ali2[$i];	}
				elsif ($ali2[$i] eq "0") { $aux[$i] = $ali1[$i];	}
				else { $aux[$i] = une($ali1[$i],$ali2[$i]); }
			}
			# resultado da uniao @aux = (6_7,1,2,3,4,5,1_6_8)
			# remove elementos alinhados da primeira posicao (sem alinhamento)
			remove_null(\@aux); # @aux = (7,1,2,3,4,5,1_6_8)
		}
		else {
			for($i=0;$i <= $#ali1;$i++) {
				$aux[$i] = intersecta($ali1[$i],$ali2[$i]); 
#				print "Interseccao de $ali1[$i] com $ali2[$i] = $aux[$i]\n";
			}
			# resultado da interseccao @aux = (7,0,2,3,4,5,8)
			if ($op =~ /^r/) { refina(\@aux,\@ali1,\@ali2,$op);	}
			# inserir 1 e 6 (que nao aparecem em ali1) no alinhamento com NULL 
			insere_null($#ali2,\@aux); # @aux = (1_6_7,0,2,3,4,5,8)
		}
		# corrige possiveis desalinhamentos gerados pela insercao de um novo item
		# num alinhamento envolvendo multipalavras
		corrige_alinhamento(\@aux);
		# prepara para impressao
		map(s/\_/ /g,@aux);
		map(s/^0$//,@aux);
		print join(" ",map($tok1[$_]." ({ ".$aux[$_]." })",0..$#aux)),"\n";
	}
	else {
		print $linha1;
	}	
	$linha1 = <IN1>;
	$linha2 = <IN2>;

}
close IN1;
close IN2;

#************************************************************************************************
#                                             SUB-ROTINAS
#************************************************************************************************
# Sub-rotina simetriza_probabilidades
# Entrada: $prob1 e $prob2 (probabilidades a serem simetrizadas)
#          $op             (metodo de simetrizacao selecionado)
# Saida: O resultado da simetrizacao de $prob1 e $prob2 de acordo com $op
# Funcao: Gera uma probabilidade resultante da simetrizacao dos dois alinhamentos. O valor resultante
# foi definido ad-hoc como:
#  - o maior valor das probabilidades se a simetrizacao corresponde ah interseccao (i) = mais preciso
#  - o menor valor das probabilidades se a simetrizacao corresponde ah uniao (u) = menos preciso
#  - a media aritmetica das probabilidades se a simetrizacao corresponde ao refinamento (r ou r1)
sub simetriza_probabilidades {
	my($prob1,$prob2,$op) = @_;
	
	if ($op eq "i") { return ($prob1 < $prob2) ? $prob2 : $prob1; }
	if ($op eq "u") { return ($prob1 < $prob2) ? $prob1 : $prob2; }
	elsif ($op =~ /^r/) { return ($prob1+$prob2)/2; }
}

# Sub-rotina converte_linha_alinhamento
# Entrada: $linha (uma string com os alinhamentos definidos para uma sentenca, no formato de GIZA)
# Saida: $ali (array com os alinhamentos)
#        $tok (array com os tokens)
# Funcao: Separa os alinhamentos definidos na string de entrada em indicacoes de alinhamento ($ali)
# e tokens ($tok)
sub converte_linha_alinhamento {
	my($linha,$ali,$tok) = @_;

	$linha =~ s/\n//; # NULL ({ 7 }) o ({ }) dente ({ 2 }) de+o ({ 3 }) mais ({ 4 }) antigo ({ 5 }) orangotango ({ 1 6 8 }) 
	$linha =~ s/\s*\}\)\s*$//; # NULL ({ 7 }) o ({ }) dente ({ 2 }) de+o ({ 3 }) mais ({ 4 }) antigo ({ 5 }) orangotango ({ 1 6 8
	@$ali = split(/\s+\}\)\s+/,$linha); # @$ali = (NULL ({ 7,o ({,dente ({ 2,de+o ({ 3,mais ({ 4,antigo ({ 5,orangotango ({ 1 6 8)
	@$tok = @$ali; 
	map(s/[^\{]+\{\s*//,@$ali); # @$ali = (7,,2,3,4,5,1 6 8) #**
	map(s/\s.+//,@$tok); # @$tok = (NULL,o,dente,de+o,mais,antigo,orangotango)
	map(s/^$/0/,@$ali); # @$ali = (7,0,2,3,4,5,1 6 8)
	map(s/ /\_/g,@$ali); # @$ali = (7,0,2,3,4,5,1_6_8)
}

# Sub-rotina inverte_sentido_alinhamento
# Entrada: $ali (array com os alinhamentos)
# Saida: $ali (alterado apos inverter os alinhamentos inicialmente em $ali)
# Funcao: Inverte a ordem dos alinhamentos em $ali. Por exemplo, se $ali[i] = j_k, entao
# $ali[j] = i e $ali[k] = i.
sub inverte_sentido_alinhamento {
	my($ali) = @_;
	my(@aux,@invertido,$pos);
	
	@invertido = ();
	$invertido[0] = join("_",grep($$ali[$_] eq "0",1..$#$ali)); # todos que estao alinhados com NULL vao para posicao 0
	if ($invertido[0] eq "") { $invertido[0] = "0"; } 
	for ($i = 1;$i <= $#$ali;$i++) { 
		@aux = ();
		$pos = 0;
		do {
		  $pos = posicao($i,$ali,$pos+1); 
		  if ($pos >= 0) { push(@aux,$pos); }
		} until ($pos < 0);
		$invertido[$i] = join("_",sort{int($a) <=> int($b)} @aux);
	}
	map(s/^$/0/,@invertido); 
	@$ali = @invertido;
}

# Sub-rotina pertence
# Entrada: $elemento (elemento para o qual se deseja retornar a posicao em $array)
#          $array    (array de elementos no qual se deseja procurar $elemento)
# Saida: 1 se $elemento pertence a $array, 0 caso contrario
# Funcao: Verfica se $elemento pertence a $array
sub pertence {
	my($elemento,$array) = @_;
	my(@aux);	
	
	@aux = grep(/^$elemento$/,@$array);
	return ($#aux >= 0);
}

# Sub-rotina posicao
# Entrada: $elemento (elemento para o qual se deseja retornar a posicao em $array)
#          $array    (array de elementos no qual se deseja procurar $elemento)
#          $offset   (posicao na qual a busca deve comecar)
# Saida: Posicao de $elemento em $array ou -1 se ele nao pertencer a $array
# Funcao: Retorna a posicao de $elemento em $array ou -1 caso ele nao ocorra
sub posicao {
	my($elemento,$array,$offset) = @_;
	my($pos,@aux);
	
	$pos = $offset;
	while ($pos <= $#$array) {
		@aux = split(/\_/,$$array[$pos]);
		if (pertence($elemento,\@aux) == 0) {	$pos++; }
		else { last; }
	}
	if ($pos <= $#$array) { return $pos; }
	return -1;
}

# Sub-rotina todas_ocorrencias
# Entrada: $elemento (elemento para o qual se deseja retornar a posicao em $array)
#          $array    (array de elementos no qual se deseja procurar $elemento)
#          $offset   (posicao na qual a busca deve comecar)
# Saida: @aux (um array com todas as posicoes de $elemento em $array)
# Funcao: Retorna um array com todas as posicoes de $elemento em $array ou um array vazio 
# caso ele nao ocorra. Simplesmente chama varias vezes a funcao posicao. Sub-rotina util
# quando se deseja obter todas as ocorrencias de um dado elemento e nao apenas uma.
sub todas_ocorrencias {
	my($elemento,$array,$offset) = @_;
	my($pos,@aux);

	@aux = ();
	$pos = posicao($elemento,$array,$offset);
	while ($pos > -1) {
		push(@aux,$pos);
		$pos = posicao($elemento,$array,$pos+1);
	}
	return @aux;	
}


# Sub-rotina seta_alinhamento
# Entrada: $i   (posicao fonte do alinhamento)
#          $j   (posicao alvo do alinhamento)
#          $ali (array de alinhamentos)
# Saida: $ali (array alterado apos inserir o alinhamento (i,j))
# Funcao: Insere o alinhamento (i,j) em $ali
sub insere_alinhamento {
	my($i,$j,$ali) = @_;
	my(@aux);

	@aux = split(/\_/,$$ali[$i]);
	@aux = grep($_ > 0,@aux);
	push(@aux,$j);
	$$ali[$i] = join("_",sort {int($a) <=> int($b)} grep($_ > 0,@aux));
}

# Sub-rotina insere_null
# Entrada: $qtd   (quantidade de elementos cujas posicoes devem aparecer pelo menos uma vez em $array)
#          $array (array de alinhamentos)
# Saida: $array alterado apos a insercao das posicoes que nao ocorriam anteriormente como alinhadas
# com NULL, ou seja, em $$array[0]
# Funcao: Insere em $$array[0] aquelas posicoes que nao aparecem em $array, ou seja, que estao alinhadas
# com NULL
sub insere_null {
	my($qtd,$array) = @_;
	
	map((posicao($_,$array,0) == -1) ? insere_alinhamento(0,$_,$array) : (),1..$qtd);
}

# Sub-rotina remove_null
# Entrada: $array (array de alinhamentos)
# Saida: $array alterado apos a remocao das posicoes que ocorrem em $array na posicao $$array[0] e em
# alguma outra diferente de 0 ja que, se elas ocorrem em alguma posicao diferente de 0 (estao alinhadas)
# nao devem ocorrer em $$array[0] (indicando que nao estao alinhadas)
# Funcao: Remove de $$array[0] aquelas posicoes que aparecem em alguma outra posicao de $array
sub remove_null {
	my($array) = @_;
	my(@aux,@aux2);
	
	@aux = ();
	@aux2 = @$array;
	shift(@aux2);
	map(posicao($_,\@aux2,0) == -1 ? push(@aux,$_) : (),split(/\_/,$$array[0]));
	$$array[0] = join("_",sort {int($a) <=> int($b)} @aux);
}

# Sub-rotina corrige_alinhamento
# Entrada: $array    (array de elementos no qual se deseja procurar $elemento)
# Saida: $array corrigido
# Funcao: Corrige possiveis desalinhamentos causados pela insercao de itens em alinhamento multipalavras
# Exemplo:
# Entrada: (35, 35_36, 36_37)
# Saida: (35_36_37, 35_36_37, 35_36_37)
sub	corrige_alinhamento {
	my($array) = @_;
	my($i,@aux,@pos,@ali);

#	print "Antes: ",join(" ",@$array),"\n"; 	
	for($i = 1;$i <= $#$array;$i++) {
		@aux = split(/\_/,$$array[$i]);
		@pos = map(todas_ocorrencias($_,$array),@aux);
		@ali = ();
		if ($#pos > 0) {
			map(pertence($_,\@ali) == 0 ? push(@ali,$_) : (),map(split(/\_/,$$array[$_]),@pos));
			@ali = sort{int($a) <=> int($b)} @ali;
			while ($#pos >= 0) { $$array[shift(@pos)] = join("_",@ali); }
		}
		
	}
#	print "Depois: ",join(" ",@$array),"\n"; exit;
}

#************************************************************************************************
#                                             UNIAO
#************************************************************************************************
# Sub-rotina une
# Entrada: $al1 (alinhamento de uma dada posicao segundo alinhamento 1)
#          $al2 (alinhamento de uma dada posicao segundo alinhamento 2)
# Saida: Uniao dos dois alinhamentos
# Funcao: Faz a uniao dos alinhamentos definidos em $al1 e $al2 quando possivel
sub une {
	my($al1,$al2) = @_;
	my(@aux,@uniao);
	
	@uniao = split(/\_/,$al1);
	@aux = split(/\_/,$al2);
	map(posicao($_,\@uniao,0) == -1 ? push(@uniao,$_) : (),@aux);
	return join("_",sort {int($a) <=> int($b)} @uniao);
}

#************************************************************************************************
#                                             INTERSECCAO
#************************************************************************************************
# Sub-rotina intersecta
# Entrada: $al1 (alinhamento de uma dada posicao segundo alinhamento 1)
#          $al2 (alinhamento de uma dada posicao segundo alinhamento 2)
# Saida: Interseccao dos dois alinhamentos, se possivel, senao 0 (indicacao de alinhamento com NULL)
# Funcao: Faz a interseccao dos alinhamentos definidos em $al1 e $al2 quando possivel ou retorna 0
# quando nao ha posicao em comum nos alinhamentos $al1 e $al2
sub intersecta {
	my($al1,$al2) = @_;
	my(@aux1,@aux2,@inter);
	
	@inter = ();
	@aux1 = split(/\_/,$al1);
	@aux2 = split(/\_/,$al2);
	map(posicao($_,\@aux1,0) >= 0 ? push(@inter,$_) : (),@aux2);
	if ($#inter >= 0) { return join("_",sort {int($a) <=> int($b)} @inter); }
	return "0";
}

#************************************************************************************************
#                                             REFINAMENTO
#************************************************************************************************
# Sub-rotina define_unicos
# Entrada: $ref  (o array com a interseccao entre os alinhamentos $ali1 e $ali2)
#          $ali1 (array com alinhamentos definidos no primeiro arquivo de entrada)
#          $ali2 (array com alinhamentos definidos no segundo arquivo de entrada)
# Saida: $unicos (array com os alinhamentos que ocorrem apenas em $ali1 ou em $ali2)
sub define_unicos {
	my($ref,$ali1,$ali2,$unicos) = @_;
	my($i,@aux1,@aux2);
	
	@$unicos = ();
	for($i=1;$i <= $#$ali1;$i++) {
		@aux1 = grep(($_ != 0) && ($$ref[$i] !~ /\_*$_\_*/),split(/\_/,$$ali1[$i]));
		@aux2 = grep(($_ != 0) && ($$ref[$i] !~ /\_*$_\_*/),split(/\_/,$$ali2[$i]));			
		push(@aux1,@aux2);
		@aux1 = sort{int($a) <=> int($b)} @aux1;
		map(s/$_/$i\_$_/,@aux1);
		push(@$unicos,@aux1);
	}
}

# Sub-rotina tenta_condicao_1
# Entrada: $i   (posicao no array de alinhamentos, fi)
#          $j   (conteudo do array de alinhamentos, ej)
#          $ref (inicialmente, o array com a interseccao entre os alinhamentos $ali1 e $ali2)
# Saida: 1 se a primeira condicao eh satisfeita, 0 caso contrario
# Funcao: Verifica se o alinhamento (i,j) satisfaz a primeira condicao do refinamento, ou seja,
# nao ha em A ($ref) alinhamento algum definido para $i ou $j
sub tenta_condicao_1 {
	my($i,$j,$ref) = @_;
	
	if (($$ref[$i] == 0) && (posicao($j,$ref,0) == -1)) { # nem fi nem ej tem alinhamento em A (inter)
		return 1;
	}
	return 0;
}

# Sub-rotina tenta_condicao_2
# Entrada: $i   (posicao no array de alinhamentos, fi)
#          $j   (conteudo do array de alinhamentos, ej)
#          $ref (array de alinhamentos refinados)
# Saida: $ref (array de alinhamentos refinados alterado para conter o alinhamento de $i para $j
# se este satisfizer as condicoes definidas no artigo de Och e Ney 2003)
# Funcao: Verifica se o alinhamento de $i para $j satisfaz as condicoes definidas em (Och & Ney, 2003)
# se satisfizer esse alinhamento eh inserido em $ref
sub tenta_condicao_2 {
	my($i,$j,$ref) = @_;
	my($hi,$hs,$vi,$vs,@aux);

	$hi = (($i > 1) && ($$ref[$i-1] =~ /\_*$j\_*/)); # verifica se vizinho horizontal inferior pertence a A (inter)
	$hs = (($i < $#$ref) && ($$ref[$i+1] =~ /\_*$j\_*/)); # verifica se vizinho horizontal superior pertence a A (inter)
	if ($$ref[$i] == 0) { $vi = $vs = 0; }
	else {
		@aux = split(/\_/,$$ref[$i]);
		$vi = (posicao($j-1,\@aux,0) >= 0); # verifica se vizinho vertical inferior pertence a A
		$vs = (posicao($j+1,\@aux,0) >= 0); # verifica se vizinho vertical superior pertence a A
	}
	if (($hi || $hs || $vi || $vs) && !(($hi || $hs) && ($vi || $vs))) { return 1; }
	return 0;
}

# Sub-rotina refina
# Entrada: $ref  (inicialmente, o array com a interseccao entre os alinhamentos $ali1 e $ali2)
#          $ali1 (array com alinhamentos definidos no primeiro arquivo de entrada)
#          $ali2 (array com alinhamentos definidos no segundo arquivo de entrada)
#          $op   (opcao de refinamento r1 - testa C1 primeiro - ou r - testa as duas condicoes juntas)
# Saida: $ref (array alterado para conter novos alinhamentos se algum satisfizer as condicoes do refinamento)
# Funcao: Retorna o alinhamento refinado (segundo artigo de Och e Ney, 2003) em $ref gerado com base nos alinhamentos
# em $ali1 e $ali2.
sub refina {
	my($ref,$ali1,$ali2,$op) = @_;
	my($i,$j,$k,@unicos,$novo);

	define_unicos($ref,$ali1,$ali2,\@unicos);
	if ($op eq "r1") {
		# A primeira condicao eh testada antes, ou seja, insere em A os alinhamentos (i,j) se i e j nao 
		# estao alinhados com ninguem em A
		for($k = 0;$k <= $#unicos;$k++) {
			($i,$j) = split(/\_/,$unicos[$k]);
			if (tenta_condicao_1($i,$j,$ref)) { 
				insere_alinhamento($i,$j,$ref);
				delete($unicos[$k]);
			}	
		}
		@unicos = grep(defined($_),@unicos);
	}

	# Insere, iterativamente (enquanto novos alinhamentos forem criados), alinhamentos
	# (i,j) se existir vizinho horizontal ou vertical mas nao os dois ao mesmo tempo!
	$novo = 1;
	while ($novo) { # novos alinhamentos estiverem sendo criados
		$novo = 0;
		for($k=0;$k <= $#unicos;$k++) {
			($i,$j) = split(/\_/,$unicos[$k]);
			if ($op eq "r1") { $novo = tenta_condicao_2($i,$j,$ref); }
			else { $novo = (tenta_condicao_1($i,$j,$ref) || tenta_condicao_2($i,$j,$ref)); }
			if ($novo) { 
				insere_alinhamento($i,$j,$ref);
				delete($unicos[$k]); 
			}
		}	
		@unicos = grep(defined($_),@unicos);
	}	
}
