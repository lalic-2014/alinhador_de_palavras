#!/usr/bin/perl

# Este programa é um software livre; você pode redistribui-lo e/ou 
# modificá-lo dentro dos termos da Licença Pública Geral GNU como 
# publicada pela Fundação do Software Livre (FSF); na versão 2 da 
# Licença, ou (na sua opnião) qualquer versão.
# Este programa é distribuído na esperança que possa ser útil, 
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer
# MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, escreva para a Fundação do Software
# Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Programa GIZA_to_PorTAl
# Entrada: 
# 	1. arquivo alinhado lexicalmente por GIZA++
# 	2. arquivo fonte alinhado sentencialmente
# 	3. arquivo alvo alinhado sentencialmente
# Saida: Arquivos alinhados por GIZA no formato do PorTAl
# Funcao: Converte a saida de GIZA no padrao do PorTAl

# As sentencas dos arquivos <entrada_fonte> e <entrada_alvo> devem ser impressas na ordem em que ocorrem, com as informações originais de alinhamento (sentenças correspondentes, tipo) e com os tokens com capitalização original. Se separa tokens foi aplicado antes de GIZA++ também deve ser aplicado aqui.
# Inserir info de alinhamento lexical aos tokens de %sfonte e %salvo e imprimir

# $ perl GIZA_to_PorTAl.pl st.A3.final ../arquivos/3.alinhado_sentencialmente/5.txt ../arquivos/3.alinhado_sentencialmente/6.txt fonte_lex.txt alvo_lex.txt -s

use strict;
use locale;
use PorTAl::Aux;

if ($#ARGV < 4) {
    print "Uso: $0 <alinhamento_giza> <entrada_fonte> <entrada_alvo> <saida_fonte> <saida_alvo> [-s]\n";
    print "Copyright (C) 2010-2012  PorTAl (www.lalic.dc.ufscar.br/portal)\n";
    exit 1;
};

my(@allexfonte,@allexalvo,@atypefonte,@atypealvo);

le_alinhamento_lexical(shift(@ARGV),\@allexfonte,\@allexalvo,\@atypefonte,\@atypealvo);

# Parametros obrigatorios de entrada
my $entradafonte = shift(@ARGV);
my $entradaalvo = shift(@ARGV);
my $saidafonte = shift(@ARGV);
my $saidaalvo = shift(@ARGV);

# Parametros opcionais de entrada
my $separa = 0; # por padrao nao separa tokens

# Se ainda tiver algum argumento verifica se ele indica como as sentencas dos arquivos de entrada foram pre-processadas
# para serem fornecidas ao GIZA++ (aplicacao do separa tokens). O mesmo processamento deve ser repetido aqui
while ($#ARGV >= 0) {
	if ($ARGV[0] eq '-s') { $separa = 1; }
	shift(@ARGV);
}

my(%sfonte,%salvo,);

# Le arquivos com alinhamento sentencial armazenando as sentencas (e suas informacoes) em %s e os alinhamentos em (%al)
my $cabf = le_sentencas($entradafonte,\%sfonte,$separa);
my $caba = le_sentencas($entradaalvo,\%salvo,$separa);

# Transforma as sentencas fonte e alvo armazenadas no campo 'sentenca' dos hashs %sfonte e %salvo em arrays de tokens
gera_array(\%sfonte);
gera_array(\%salvo);

une_alinhamentos(\@allexfonte,\@allexalvo,\@atypefonte,\@atypealvo,\%sfonte,\%salvo);

imprime(\%sfonte,$saidafonte,$cabf);
imprime(\%salvo,$saidaalvo,$caba);

sub le_alinhamento_lexical {
	my($arq,$arrayfonte,$arrayalvo,$arrayatypefonte,$arrayatypealvo) = @_;
	my(@al,@alalvo,$i,@aux,@atf,@ata,$qtdf,$qtda);
	
	# Le alinhamentos lexicais de GIZA++
	open(ARQ,$arq) or erro(1,$arq);
	while (<ARQ>) {
		if (/^NULL/) { # So me interessa as linhas que comecam com 'NULL' pois nessas que estao os alinhamentos
			@al = split(/\s+\}\)\s*/);
			map(s/^.+\(\{\s*(.*)/$1/,@al); #*# Ex: @al = (,1 4,2,3,,5,6)
			@alalvo = @atf = @ata = (); 
			# Processa alinhamentos de omissao alvo, ou seja, alvo sem alinhamento lexical 0:1 (NULL)
			map($alalvo[$_-1] = 0,split(/ /,shift(@al))); # Insere 0 na posicao de cada um desses tokens. Ex: @al = (1 4,2,3,,5,6)
			map(s/ /\_/g,@al); 		# Ex: @al = (1_4,2,3,,5,6)
			map(s/^$/0/,@al); 		# Ex: @al = (1_4,2,3,0,5,6)

			for($i=0;$i <= $#al;$i++) { # Ex: @al = (1_4,2,3,0,5,6)
				if ($al[$i] ne "0") {
					@aux = split(/\_/,$al[$i]);
					# Calcula o tipo e alinhamento fonte-alvo
					$qtda = $#aux+1;
					$qtdf = scalar(grep(/^$al[$i]$/,@al));
					$atf[$i] = $qtdf.'-'.$qtda;			# e armazena na posicao do token fonte
					while ($#aux >= 0) {
						if ($alalvo[$aux[0]-1] =~ /\d/) { $alalvo[$aux[0]-1] .= '_'; }
						$ata[$aux[0]-1] = $qtdf.'-'.$qtda;	# e armazena na posicao do token alvo
						$alalvo[shift(@aux)-1] .= $i+1;
					}
#					map(($alalvo[$_-1] =~ /\d/) ? $alalvo[$_-1] .= '_'.($i+1) : $alalvo[$_-1] = $i+1,split(/\_/,$al[$i])); # 
				}
			}

			map($atf[$_] =~ s/^$/1-0/,0..$#al);
			push(@$arrayfonte,[@al]); 			# Ex: @$arrayfonte= ((1_4,2,3,0,5,6))
			push(@$arrayatypefonte,[@atf]);	# Ex: @$arrayatypefonte= ((1-2,1-1,1-1,1-0,1-1,1-1))

			map($ata[$_] =~ s/^$/0-1/,0..$#alalvo);
			push(@$arrayalvo,[@alalvo]);  	# Ex: @$arrayalvo= ((1,2,3,1,5,6))
			push(@$arrayatypealvo,[@ata]);  	# Ex: @$arrayatypealvo= ((1-2,1-1,1-2,1-2,1-1,1-1))

#			print STDERR "Fonte = ",join(" ",map(($_+1).'='.$al[$_],0..$#al));
#			print STDERR "\nAlvo = ",join(" ",map(($_+1).'='.$alalvo[$_],0..$#alalvo)),"\n\n"; exit;
		}
	}
	close ARQ;
}

sub une {
	my($allex,$atype,$ids,$hashsent) = @_;
	my(@aux,@at,$i,@bkp);

	@bkp = @$ids;
	@aux = @{shift(@$allex)};	# Contem os alinhamentos dessa sentenca. Ex: @aux = (1_4,2,3,0,5,6)
	@at = @{shift(@$atype)};	# Contem os tipos de alinhamentos dessa sentenca. Ex: @at = (1-2,1-1,1-1,1-0,1-1,1-1)

	while ($#aux >= 0) { # Enquanto houver tokens alinhados, inserir alinhamentos nas sentencas	
		for($i=0;$i <= $#{$$hashsent{$$ids[0]}{'sentenca'}};$i++) { # Para cada token dessa sentenca
			$$hashsent{$$ids[0]}{'sentenca'}[$i] .= ":".shift(@aux).'|'.shift(@at); # Concatena um alinhamento lexical
		}
		if ($#aux >= 0) { # Ainda restam tokens alinhados, buscar nova sentenca
			shift(@$ids);
		}
	}
	@$ids = @bkp;
}

# Sub-rotina aplica_deslocamento
# Entrada:
#   $ids   - array com ids das sentencas ALVO
#   $sents - array com as sentencas ALVO (array de tokens em 'sentenca')
# Saida:
#   $allex - array com alinhamentos lexicais FONTE, ou seja, o array contem ids ALVO
# Funcao: Aplica um deslocamento nos ids de tokens em $allex. Para isso
#   1. Para cada sentenca ALVO envolvida no alinhamento que se deseja atualizar (cada indice em $ids)
#   2. Calcula os limites minimo ($min) e maximo ($max) dos indices de tokens da sentenca ALVO em questao
#   3. Calcula quantos tokens vieram antes, ou seja, qual o tamanho total das sentencas que a precedeu = $des
#   4. Atualiza esse deslocamento para subtrair o deslocamento que a juncao das sentencas para entrada de GIZA++ pode ter inserido ($desali)
#   5. Aplica o deslocamento a todos os indices de tokens em $allex referentes a essa sentenca, ou seja, entre $min e $max
sub aplica_deslocamento {
	my($ids,$sents,$allex) = @_;
	my($i,$j,@chaves,$des,$desali,$id,@aux,$min,$max);

	@aux = @{$$allex[0]};
	# Armazena todos os IDs das sentencas alvo
	@chaves = sort {compara($a,$b)} keys %$sents; 
	$desali = 0; # Armazena o deslocamento dentro de $allex
	for($i=0;$i <= $#$ids;$i++) { # PASSO 1 -- Para cada sentenca alvo -- funcionou 1:2 mas nao esta funcionando 2:1
		$des = $j = 0;
		# PASSO 2 -- Calcula os limites minimo e maximo de tokens dessa sentenca
		$min = $desali+1;
		$max = $desali+$#{$$sents{$$ids[$i]}{'sentenca'}}+1; 
		# PASSO 3 -- Calcula quantos tokens existem antes da sentenca em $$ids[$i]
		while ($chaves[$j] ne $$ids[$i]) { # Soma o numero de tokens (deslocamento) das sentencas que a precedem
			$des += $#{$$sents{$chaves[$j++]}{'sentenca'}}+1;
		}
		$des -= $desali; # PASSO 4 -- Atualiza deslocamento, pois se varias sentencas foram unidas para entrada de GIZA++ entao ja ha deslocamento
		# PASSO 5 -- Aplica o deslocamento as tokens entre $min e $max -- o @ eh so para marcar os indices que ja foram atualizados
		map($_ = join("_",map(($_ !~ /@/) && ($_ >= $min) && ($_ <= $max) ? ($_+$des).'@' : $_,split(/_/))),@aux); 
		$desali += $#{$$sents{$$ids[$i]}{'sentenca'}}+1;
	}
	map(s/@//g,@aux); #//
	@{$$allex[0]} = @aux;
}

sub une_alinhamentos {
	my($allexfonte,$allexalvo,$atypefonte,$atypealvo,$sentfonte,$sentalvo) = @_;
	my(@chaves,@idsalvo,@idsfonte,$i,@alvosomissao);

	# Armazena os ids das sentencas fonte ordenados de acordo com seus identificadores de sentencas e paragrafos
	@chaves = sort {compara($a,$b)} keys %$sentfonte;    

	# Armazena os ids das sentencas alvo ordenados de acordo com seus identificadores de sentencas e paragrafos
	@alvosomissao = sort {compara($a,$b)} keys %$sentalvo;    

	while ($#chaves >= 0) { # Enquanto houver sentenca fonte a ser processada
		@idsalvo = ();
		@idsfonte = (shift(@chaves)); # Pelo menos a primeira sentenca fonte sera processada
		@idsalvo =  split(",",$$sentfonte{$idsfonte[0]}{'crr'}); # Armazena tambem os ids alvo das sentencas alvo alinhadas
		$i = 0;
		while ($i <= $#idsalvo) { # Busca possiveis outros ids fonte de um alinhamento multiplo n:m com n > 1
			map(insere_array($_,\@idsfonte),split(",",$$sentalvo{$idsalvo[$i++]}{'crr'}));		
		}      
	 	if ($#idsalvo >= 0) { # Se o alinhamento lexical foi gerado 
			# Por precaucao, ordeno ids fonte e alvo
			# No alinhamento automatico nao eh necessario, mas se alguem mudar a ordem manualmente, sera
			@idsfonte = sort {compara($a,$b)} @idsfonte;
			@idsalvo = sort {compara($a,$b)} @idsalvo;
			aplica_deslocamento(\@idsalvo,$sentalvo,$allexfonte); 	# Aplica deslocamento nos ids dos tokens -- FONTE
			aplica_deslocamento(\@idsfonte,$sentfonte,$allexalvo); 	# Aplica deslocamento nos ids dos tokens -- ALVO
			une($allexfonte,$atypefonte,\@idsfonte,$sentfonte); 		# Une a informacao de alinhamento lexical nas sentencas -- FONTE
			une($allexalvo,$atypealvo,\@idsalvo,$sentalvo);				# Une a informacao de alinhamento lexical nas sentencas -- ALVO
		}
		else { # O alinhamento sentencial eh 1:0 - sentenca fonte nao foi alinhada
			map($_ !~ /\:\d+(\_\d+)*$/ ? $_ .= ":0|10" : (),@{$$sentfonte{$idsfonte[0]}{'sentenca'}});	# todos os tokens fonte recebem :0
		}
		@chaves = grep(pertence($_,\@idsfonte) == 0,@chaves); # Remove de @chaves os idsfonte processados
		@alvosomissao = grep(pertence($_,\@idsalvo) == 0,@alvosomissao); # Remove de @alvosomissao os idsalvo processados
	}	
	while ($#alvosomissao >= 0) { # Enquanto houver sentenca alvo a ser processada
		# O alinhamento sentencial eh 0:1 - sentenca alvo nao foi alinhada
		map($_ !~ /\:\d+(\_\d+)*$/ ? $_ .= ":0|01" : (),@{$$sentalvo{shift(@alvosomissao)}{'sentenca'}});	# todos os tokens alvo recebem :0
	}
}

# Subrotina imprime
sub imprime {
	my($hashsent,$arq,$cab) = @_;
	my(@ids) = sort {compara($a,$b)} keys %$hashsent;
	my$cabs;

	zera_arquivo($arq);
	imprime_cabecalho($arq,$cab);

	while ($#ids >= 0) {
		$cabs = "<s id=\"".$ids[0]."\" crr=\"".$$hashsent{$ids[0]}{'crr'}."\" atype=\"".$$hashsent{$ids[0]}{'atype'}."\">";
		imprime_sentenca_etiquetada_alinhada($arq,$cabs,\@{$$hashsent{$ids[0]}{'sentenca'}});
		shift(@ids);
	}

	imprime_fim($arq);
}

sub pertence {
	my($elemento,$array) = @_;
		
	$elemento = quotemeta($elemento);
	return grep(/^$elemento$/,@$array);	
}


sub insere_array {
	my($elemento,$array) = @_;
	
	if (pertence($elemento,$array) == 0) { push(@$array,$elemento); }	
}

sub gera_array {
	my($hashsent) = @_;
	my(@chaves) = sort {compara($a,$b)} keys %$hashsent;
	my $des = 1;

	while ($#chaves >= 0) {
		if ($$hashsent{$chaves[0]}{'sentenca'} =~ /<w id=/) { # Sentenca ja etiquetada morfossintaticamente
			$$hashsent{$chaves[0]}{'sentenca'} = [split(/<\/w> */,$$hashsent{$chaves[0]}{'sentenca'})];
			map($_ .= "</w>",@{$$hashsent{$chaves[0]}{'sentenca'}});
		}
		else { # Sentenca nao etiquetada morfossintaticamente
			$$hashsent{$chaves[0]}{'sentenca'} = [split(/ +/,$$hashsent{$chaves[0]}{'sentenca'})];
			map(${$$hashsent{$chaves[0]}{'sentenca'}}[$_] = "<w id=\"".($_+$des)."\">".${$$hashsent{$chaves[0]}{'sentenca'}}[$_]."</w>",0..$#{$$hashsent{$chaves[0]}{'sentenca'}});
			$des += $#{$$hashsent{$chaves[0]}{'sentenca'}}+1;
		}
		shift(@chaves);
	}
}

