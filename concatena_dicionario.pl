#!/usr/bin/perl

# Este programa é um software livre; você pode redistribui-lo e/ou 
# modificá-lo dentro dos termos da Licença Pública Geral GNU como 
# publicada pela Fundação do Software Livre (FSF); na versão 2 da 
# Licença, ou (na sua opnião) qualquer versão.
# Este programa é distribuído na esperança que possa ser útil, 
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer
# MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, escreva para a Fundação do Software
# Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Programa concatena_dicionario
# Entrada: 
# 1. arquivo fonte a ser alinhado por GIZA++
# 2. arquivo alvo a ser alinhado por GIZA++
# 3. arquivo com dicionario contendo pares de tokens fonte-alvo e suas frequencias de co-ocorrencia
#    calculadas em treinamento anterior (esse arquivo DEVE ser gerado por gera_dicionario.pl a partir
#    dos arquivos de saida do GIZA++ treinado com o corpus todo)
# Saida: Arquivos fonte e alvo a serem alinhados por GIZA++ concatenados com os tokens do dicionario que
# ocorrem nos arquivos a serem alinhados. Para cada par de tokens no dicionario
#   token_fonte token_alvo frequencia_par
# se token_fonte E token_alvo ocorrem nos arquivos fonte e alvo a serem alinhados por GIZA++ entao a eles
# sao concatenados frequencia_par vezes o token_fonte e frequencia_par vezes o token_alvo, respectivamente
# Funcao: Concatena tokens do dicionario de entrada nos arquivos a serem alinhados, aumentando suas frequencias
# e, consequentemente, a probabilidade de seus alinhamentos

# $ perl concatena_dicionario.pl GIZA_F.txt GIZA_A.txt dicionario_pt-en.txt GIZA_F_dic.txt GIZA_A_dic.txt

use strict;
use locale;

if ($#ARGV < 4) {
    print "Uso: $0 <arquivo_fonte> <arquivo_alvo> <dicionario> <saida_fonte> <saida_alvo> [<freq_max>]\n";
    print "freq_max: numero maximo de vezes que o par do dicionaro sera impresso nos arquivos a serem alinhados (default = 100)\n";
    print "Copyright (C) 2010-2012  PorTAl (www.lalic.dc.ufscar.br/portal)\n";
    exit 1;
};

my($entradafonte,$entradaalvo,$arqdic,$saidafonte,$saidaalvo,%tfonte,%talvo,@dic,$tf,$ta,$freq,$i);

$entradafonte = shift(@ARGV);
$entradaalvo  = shift(@ARGV);
$arqdic       = shift(@ARGV);
$saidafonte   = shift(@ARGV);
$saidaalvo    = shift(@ARGV);

my $freqmax = 100;
if ($#ARGV >= 0) {
	$freqmax = shift(@ARGV);
}

	# PASSO 1 -- Armazena os tokens fonte e alvo em dois hashes que tem como chave o token e como valor 0
   # Tambem ja imprime as sentencas dos arquivos de entrada nos arquivos de saida
	le_tokens($entradafonte,$saidafonte,\%tfonte);
	le_tokens($entradaalvo,$saidaalvo,\%talvo);

	# PASSO 2 -- Le arquivo contendo pares de tokens e a frequencia de cada par no formato: token_fonte token_alvo freq
	# e imprime token_fonte no arquivo $saidafonte, token_alvo no arquivo $saidaalvo freq vezes
	open(DIC,$arqdic) or die "Nao eh possivel abrir o arquivo $arqdic\n";
	@dic = <DIC>; 	# Ex.: NULL the 12014
	close DIC;

	# PASSO 3 -- Filtra os pares de tokens do dicionario para restar apenas aqueles que efetivamente ocorrem nos arquivos a serem alinhados
	@dic = grep(/^([^\s]+) .+/ && defined($tfonte{$1}),@dic); # Mantem em @dic apenas as linhas iniciadas com um token fonte do arquivo a ser alinhado
	@dic = grep(/^[^\s]+ ([^\s]+) .+/ && defined($talvo{$1}),@dic); # Mantem em @dic apenas as linhas que contem um token alvo do arquivo a ser alinhado

	# PASSO 4 -- Concatena nos arquivos de saida os pares de tokens do dicionario, no numero de vezes de
	# sua frequencia de co-ocorrencia
	open(OUTF,">>$saidafonte") or die "Nao eh possivel abrir o arquivo $saidafonte\n";
	open(OUTA,">>$saidaalvo") or die "Nao eh possivel abrir o arquivo $saidaalvo\n";
	while ($#dic >= 0) {
		($tf,$ta,$freq) = split(/ /,shift(@dic));
		if ($freq > $freqmax) { $freq = $freqmax; }
		for($i=0;$i < $freq;$i++) {
			print OUTF $tf,"\n";
			print OUTA $ta,"\n";
		}
	}
	close OUTA;
	close OUTF;

# Sub-rotina le_tokens
# Le as linhas de $arqentrada e as imprime em $arqsaida armazenando os tokens que contem pelo menos 1 
# caractere alfanumerico (exclui caracteres de pontuacao) em $hash para serem usados no filtro dos 
# pares do dicionario (passo 3)
sub le_tokens {
	my($arqentrada,$arqsaida,$hash) = @_;

	open(IN,$arqentrada) or die "Nao eh possivel abrir o arquivo $arqentrada\n";
	open(OUT,">$arqsaida") or die "Nao eh possivel abrir o arquivo $arqsaida\n";
	while (<IN>) {
		print OUT $_;
		map($$hash{$_} = 0,grep(/\w/,split(/ /))); # Armazena apenas tokens que contem pelo menos 1 caractere alfanumerico
	}
	close OUT;
	close IN;
}

