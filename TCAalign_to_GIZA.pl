#!/usr/bin/perl

# Este programa é um software livre; você pode redistribui-lo e/ou 
# modificá-lo dentro dos termos da Licença Pública Geral GNU como 
# publicada pela Fundação do Software Livre (FSF); na versão 2 da 
# Licença, ou (na sua opnião) qualquer versão.
# Este programa é distribuído na esperança que possa ser útil, 
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer
# MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, escreva para a Fundação do Software
# Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Programa TCAaling_to_GIZA (antigo TCAalign_to_snum)
# Entrada: <entrada_fonte>: arquivo de entrada fonte
#          <entrada_alvo> : arquivo de entrada alvo
#          <saida_fonte>  : arquivo de saida fonte
#          <saida_alvo>   : arquivo de saida alvo
#          -m             : opcao para converter palavras para minusculas (aumenta as chances de alinhamento correto)
#          -s             : opcao para separar os tokens, por exemplo, inserindo espaco apos as virgulas, senao vai alinhar como se fosse uma coisa so
# Saida: Dois arquivos (fonte e alvo) com as sentencas impressas na ordem em que estao alinhadas
# Funcao: A partir de um par de textos sentencialmente alinhados gera dois arquivos com as sentencas (fonte e alvo) na
# ordem em que estao alinhadas.

# IMPORTANTE: Os alinhamentos sentenciais de omissao nao sao impressos e os alinhamentos n:m (n >= 1 ou m >= 1) sao unidos

use strict;
use locale;
use PorTAl::Aux;

if ($#ARGV < 2) {
    print "Uso: $0 <entrada_fonte> <entrada_alvo> <saida_fonte> <saida_alvo> [-m] [-s]\n";
    print "Copyright (C) 2010-2012  PorTAl (www.lalic.dc.ufscar.br/portal)\n";
    exit;
};

# Parametros obrigatorios de entrada
my $entradafonte = shift(@ARGV);
my $entradaalvo = shift(@ARGV);
my $saidafonte = shift(@ARGV);
my $saidaalvo = shift(@ARGV);

# Parametros opcionais de entrada
my($minuscula,$separa);

$minuscula = $separa = 0; # por padrao nao converte para minuscula nem separa tokens
 
while ($#ARGV >= 0) { 
	if ($ARGV[0] eq '-m') { $minuscula = 1; }
	elsif ($ARGV[0] eq '-s') { $separa = 1; }
	shift(@ARGV);
}

processa($entradafonte,$entradaalvo,$saidafonte,$saidaalvo,$minuscula,$separa);

# Para cada sentenca fonte imprime as sentencas fonte e alvo alinhadas em seus respectivos arquivos de saida
sub processa {
	my($arqfonte,$arqalvo,$saidafonte,$saidaalvo,$minuscula,$separa) = @_;
	my(%sfonte,%salvo,@chaves,@idsfonte,@idsalvo,$i);

	le_sentencas($arqfonte,\%sfonte,$separa);
	le_sentencas($arqalvo,\%salvo,$separa);  
  
	zera_arquivo($saidafonte);
	zera_arquivo($saidaalvo);

	@chaves = sort {compara($a,$b)} keys %sfonte;
	while ($#chaves >= 0) {   
		$idsfonte[0] = shift(@chaves);
		@idsalvo = split(",",$sfonte{$idsfonte[0]}{'crr'});
		if ($#idsalvo >= 0) { # so busca sentencas alvo alinhadas se alinhamento eh diferente de 1:0, ou seja crr eh diferente de ""
			$i = 0;
			@idsfonte = ();
			while ($i <= $#idsalvo) {
				map(insere_array($_,\@idsfonte),split(",",$salvo{$idsalvo[$i++]}{'crr'}));		
			}      
			if (($#idsfonte >= 0) && ($#idsalvo >= 0)) {
		    	imprime(\@idsfonte,\%sfonte,$saidafonte,$minuscula);      
		    	imprime(\@idsalvo,\%salvo,$saidaalvo,$minuscula);     
			}
		}
		map(delete $sfonte{$_},@idsfonte);        
		@chaves = sort {compara($a,$b)} keys %sfonte;         
	}  
  return 1;
}

  
# Subrotina imprime
# Entrada: $ids (array com os identificadores das sentencas a serem impressas)
#	   $sentencas (hash com as todas as sentencas)
#	   $arqsaida (arquivo no qual as sentencas serao impressas)
# Saida: Sentencas impressas no arquivo $arqsaida
# Funcao: Imprime as sentencas em $ids no arquivo $arqsaida.
sub imprime {
	my($ids,$sentencas,$arqsaida,$minuscula) = @_;
	my $saida = join(" ",map($$sentencas{$_}{'sentenca'},@$ids));
    
	if ($#$ids < 0 ){ return 0; }
	if ($minuscula) { 
    $saida = lc($saida); 
    $saida =~ tr/ÁÉÍÓÚÜÂÊÔÀÃÕÇÑ/áéíóúüâêôàãõçñ/;
  }     
	imprime_sentenca_limpa($arqsaida,$saida);
}

sub pertence {
	my($elemento,$array) = @_;
		
	$elemento = quotemeta($elemento);
	return grep(/^$elemento$/,@$array);	
}


sub insere_array {
	my($elemento,$array) = @_;

	if (pertence($elemento,$array) == 0) { push(@$array,$elemento); }	
}

