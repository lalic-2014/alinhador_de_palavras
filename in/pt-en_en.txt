<text lang="en" id="en">
<p>
<s id="en.1.1" crr="pt.1.1" atype="t1:1">The teeth of the oldest orangutan</s>
</p>
<p>
<s id="en.2.1" crr="pt.2.1" atype="t1:1">A new species of hominid found in Thailand, with an estimated age of 12 million years, has become the most distant relative of today's orangutans ( Pongo pygmaeus ).</s>
<s id="en.2.2" crr="pt.2.2" atype="t1:1">A group of French researchers connected with the European Synchrotron Radiation Facility (ESRF) arrived at this conclusion by comparing the 18 teeth of the fossil with the dentition of other ancient primates.</s>
</p>
<p>
<s id="en.3.1" crr="pt.3.1" atype="t1:1">By means of a technique called microtomography, they made three-dimensional models of the structure of each tooth and of the lower jawbone of the male and of the female of the new species, baptized as Lufengpithecus chiangmuanensis , with a resolution of 1 millionth of a meter.</s>
</p>
<p>
<s id="en.4.1" crr="pt.3.2" atype="t1:1">"We will never be certain that we are dealing with a direct ancestor, but it is something very close", says Jean-Jacques Jaeger, a paleontologist from the University of Montpellier, in France, and one of the authors of this work, published in Nature .</s>
<s id="en.4.2" crr="pt.3.3" atype="t1:1">The hypothesis gains strength from the fact that there was fossilized pollen together with the teeth, suggesting that the new species lived in a tropical forest, just like today's orangutans.</s>
</p>
</text>
